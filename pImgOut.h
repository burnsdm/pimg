//
// Created by david on 2/5/16.
//

#ifndef PIMG_PIMGOUT_H
#define PIMG_PIMGOUT_H

#include <itkImage.h>

template<typename PixelType,unsigned int Dim>
class pImgOut {
public:

    // constructor/destructor
    pImgOut();
    ~pImgOut();

    // python interface outputs
    void CopyOutput(PixelType* scalars_cpout, int sz1, int sz2, int sz3);
    void GetOutputPtr(PixelType** scalars_out, int* sz1, int* sz2, int* sz3);
    void GetSpacing(double spacing_out[3]);
    void GetOrigin(double origin_out[3]);
    void GetDirections(double directions_out[3][3]);

    // C++ interface (do not copy into SWIG file)
    typedef typename itk::Image<PixelType,Dim> ImageType;
    void SetOutputImage(typename ImageType::Pointer img);
    void UpdateOutputImage(){img_out->Update();}
    void OverideOrigin(typename ImageType::PointType origin_ovr){this->origin = origin_ovr;}
    void OverideDirections(typename ImageType::DirectionType dir_ovr){this->directions = dir_ovr;}
    void OverideSpacing(typename ImageType::SpacingType spacing_ovr){this->spacing = spacing_ovr;}

protected:
    typename ImageType::Pointer img_out;
    typename ImageType::PointType origin;
    typename ImageType::DirectionType directions;
    typename ImageType::SpacingType spacing;


};

#include "pImgOut.cpp"

#endif //PIMG_PIMGOUT_H
