//
// Created by david on 2/4/16.
//

#include "pImg.h"
#include <itkGradientMagnitudeImageFilter.h>
#include <itkRescaleIntensityImageFilter.h>

template<typename PixelType,unsigned int Dim>
pImg<PixelType,Dim>::pImg()
{
    this->img_in = ImageType::New();
    this->img_out = ImageType::New();
    this->importFilter = ImportFilterType::New();
};

template<typename PixelType,unsigned int Dim>
pImg<PixelType,Dim>::~pImg(){} // only data at present is smart pointers

template<typename PixelType,unsigned int Dim>
void pImg<PixelType,Dim>::SetScalars(PixelType* scalars, int sz1, int sz2, int sz3)
{
    /* Assigns the img_in pixel buffer to scalar array (no copy)
       numpy uses reverse indexing, but c-style memory contiguity (i think)
       2D and 3D slicer images are 3D matrices */\

    typename ImportFilterType::SizeType imSize;
    imSize[0] = sz3;                     // X coordinate
    imSize[1] = sz2;                     // Y coordinate
    if (Dim == 3)
        imSize[2] = sz1;
    else if (Dim == 2 && sz1 != 1)    {  // Z coordinate
        std::cerr << "Invalid image dimensions for npImg<PixelType,2>::SetScalars " << std::endl;
        return;
    }

    typename ImportFilterType::IndexType start;
    start.Fill( 0 );
    typename ImportFilterType::RegionType region;
    region.SetIndex(start);
    region.SetSize(imSize);
    this->importFilter->SetRegion(region);

    typename ImageType::PointType origin;
    origin.Fill(0.0);
    this->importFilter->SetOrigin(origin);

    typename ImageType::SpacingType spacing;
    spacing.Fill(1.0);
    this->importFilter->SetSpacing(spacing);

    const bool importImageFilterWillOwnTheBuffer = false;
    this->importFilter->SetImportPointer(scalars, sz1*sz2*sz3,
                                         importImageFilterWillOwnTheBuffer);

    this->img_in=this->importFilter->GetOutput();
    this->importFilter->Update();
}


template<typename PixelType,unsigned int Dim>
void pImg<PixelType,Dim>::SetInputSpacing(double* spacing, int sz)
{
    /* Sets the spacing of the input image */
    if (sz < Dim)
    {
        std::cerr<<"pImg::SetInputSpacing spacing arg invalid size " <<std::endl;
        return;
    }
    typename ImageType::SpacingType imgSpacing;
    for (int i = 0; i < Dim; i++)
        imgSpacing[i] = spacing[i];
    this->img_in->SetSpacing(imgSpacing);
}

template<typename PixelType,unsigned int Dim>
void pImg<PixelType,Dim>::SetInputOrigin(double* origin, int sz)
{
    /* Sets the origin of the input image */
    if (sz < Dim)
    {
        std::cerr<<"pImg::SetInputOrigin spacing arg invalid size " <<std::endl;
        return;
    }
    typename ImageType::PointType imgOrigin;
    for (int i = 0; i < Dim; i++)
        imgOrigin[i] = origin[i];
    this->img_in->SetOrigin(imgOrigin);
}

template<typename PixelType,unsigned int Dim>
void pImg<PixelType,Dim>::CopyOutput(PixelType* scalars_cpout, int sz1, int sz2, int sz3)
{
    /* Copies pixel buffer from img_out into scalars */

    typename ImageType::SizeType imSize = this->img_out->GetLargestPossibleRegion().GetSize();
    if (imSize[0] != sz3 || imSize[1] != sz2 || (Dim == 2 && sz1 != 1) || (Dim == 3 && imSize[2] != sz1))
    {
        std::cerr<<"pImg::CopyOutput - invalid output image dimensions..." <<std::endl;
        std::cerr<<"Py: [" <<sz1 <<"," <<sz2 <<"," <<sz3 <<"]" <<std::endl;
        std::cerr<<"Cpp: " <<imSize <<std::endl;
        return;
    }

    memcpy(scalars_cpout,this->img_out->GetBufferPointer(),sizeof(PixelType)*sz1*sz2*sz3);
}

template<typename PixelType,unsigned int Dim>
void pImg<PixelType,Dim>::GetOutputPtr(PixelType** scalars_out, int* sz1, int* sz2, int* sz3)
{
    /* Returns reference to scalars for img_out - BEWARE if object goes out of scope so does the returned pointer */
    typename ImageType::SizeType imSize = this->img_out->GetLargestPossibleRegion().GetSize();
    *sz3=imSize[0];
    *sz2=imSize[1];
    if (Dim == 3)
        *sz1=imSize[2];
    else if (Dim == 2)
        *sz1 = 1;

    typename ImageType::PixelContainer* container;
    container = this->img_out->GetPixelContainer();
    container->SetContainerManageMemory(false);
    PixelType* pixBuff = container->GetImportPointer();
    *scalars_out = pixBuff;
}


template<typename PixelType,unsigned int Dim>
void pImg<PixelType,Dim>::GetOutputSpacing(double spacing_out[3])
{
    /* Returns output image spacing */
    typename ImageType::SpacingType spacing = this->img_out->GetSpacing();
    spacing_out[0] = spacing[0];
    spacing_out[1] = spacing[1];
    if (Dim == 3)
        spacing_out[2] = spacing[2];
    else if (Dim == 2)
        spacing_out[2] = 1;
}


template<typename PixelType,unsigned int Dim>
void pImg<PixelType,Dim>::GetOutputOrigin(double origin_out[3])
{
    /* Returns output image origin */
    typename ImageType::PointType origin = this->img_out->GetOrigin();
    origin_out[0] = origin[0];
    origin_out[1] = origin[1];
    if (Dim == 3)
        origin_out[2] = origin[2];
    else if (Dim == 2)
        origin_out[2] = 0.0;
}

template<typename PixelType,unsigned int Dim>
void pImg<PixelType,Dim>::MagGrad()
{
    /* Returns gradient magnitude scaled linear over the dynamic range */
    typename itk::GradientMagnitudeImageFilter<ImageType,ImageType>::Pointer mgFilter = itk::GradientMagnitudeImageFilter<ImageType,ImageType>::New();
    mgFilter->SetInput(this->img_in);

    typename itk::RescaleIntensityImageFilter<ImageType,ImageType>::Pointer scaleFilter = itk::RescaleIntensityImageFilter<ImageType,ImageType>::New();
    PixelType pixMax = std::numeric_limits<PixelType>::max();
    PixelType pixMin = std::numeric_limits<PixelType>::min();
    scaleFilter->SetOutputMinimum(pixMin);
    scaleFilter->SetOutputMaximum(pixMax);
    scaleFilter->SetInput(mgFilter->GetOutput());
    scaleFilter->Update();

    this->img_out = scaleFilter->GetOutput();
}