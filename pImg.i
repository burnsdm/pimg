%module pImg
%{
#define SWIG_FILE_WITH_INIT
#include "pImg.h"
#include "pImg.cpp"
#include "pImgIn.h"
#include "pImgOut.h"
%}

%include "numpy.i"

%init %{
import_array();
%}

%apply (double* IN_ARRAY1, int DIM1) {(double* spacing, int sz),(double* origin, int sz)}
%apply (double* IN_ARRAY2, int DIM1, int DIM2) {(double* directions, int sz1, int sz2)}

template<typename PixelType,unsigned int Dim>
class pImg {
public:

    // constructor/destructor
    pImg();
    ~pImg();

    // python interface inputs
    void SetScalars(PixelType * scalars, int sz1, int sz2, int sz3);
    void SetInputSpacing(double* spacing, int sz);
    void SetInputOrigin(double* origin, int sz);

    // python interface outputs
    void CopyOutput(PixelType* scalars_cpout, int sz1, int sz2, int sz3);
    void GetOutputPtr(PixelType** scalars_out, int* sz1, int* sz2, int* sz3);
    void GetOutputSpacing(double spacing_out[3]);
    void GetOutputOrigin(double origin_out[3]);

    // example
    void MagGrad();
};

template<typename PixelType,unsigned int Dim>
class pImgIn {
public:

    // constructor/destructor
    pImgIn();
    ~pImgIn();

    // python interface inputs
    void SetScalars(PixelType* scalars, int sz1, int sz2, int sz3);
    void SetInputSpacing(double* spacing, int sz);
    void SetInputOrigin(double* origin, int sz);
    void SetInputDirections(double* directions, int sz1, int sz2);
};

template<typename PixelType,unsigned int Dim>
class pImgOut {
public:

    // constructor/destructor
    pImgOut();
    ~pImgOut();

    // python interface outputs
    void CopyOutput(PixelType* scalars_cpout, int sz1, int sz2, int sz3);
    void GetOutputPtr(PixelType** scalars_out, int* sz1, int* sz2, int* sz3);
    void GetSpacing(double spacing_out[3]);
    void GetOrigin(double origin_out[3]);
    void GetDirections(double directions_out[3][3]);
};

// Generic typemap for outputting on return an array of doubles
%typemap(in,numinputs=0) double[ANY] (double temp[$1_dim0]) {
    memset(temp, 0, sizeof temp);
    $1 = temp;
}
%typemap(argout) double[ANY] {
    int i;
    $result = PyList_New($1_dim0);
    for (i = 0; i < $1_dim0; i++) {
        PyObject *o = PyFloat_FromDouble((double) temp$argnum[i]);
        PyList_SetItem($result,i,o);
    }
}

%apply (double ARGOUT_ARRAY2[ANY][ANY]){(double directions_out[3][3])}

/* SWIG macro expansion to force instantiation of relevant types, currently only 2D/3D images supported
   Comment out irrelevant types for faster compiling
   template expansion must follow all apply directives in the macro */

%define TEMPLATE_HELPER(Suffix,ImgDim, PixType...)
%apply (PixType * IN_ARRAY3, int DIM1, int DIM2, int DIM3){(PixType * scalars, int sz1, int sz2, int sz3)}
%apply (PixType * INPLACE_ARRAY3, int DIM1, int DIM2, int DIM3){(PixType * scalars_cpout, int sz1, int sz2, int sz3)}
%apply (PixType ** ARGOUTVIEW_ARRAY3, int* DIM1, int* DIM2, int* DIM3){(PixType ** scalars_out, int* sz1, int* sz2, int* sz3)}


%template(pImg_ ## Suffix) pImg<PixType,ImgDim>;
%template(pImgIn_ ## Suffix) pImgIn<PixType,ImgDim>;
%template(pImgOut_ ## Suffix) pImgOut<PixType,ImgDim>;
%enddef

TEMPLATE_HELPER(char2, 2, char)
TEMPLATE_HELPER(char3, 3, char)
TEMPLATE_HELPER(uchar2, 2, unsigned char)
TEMPLATE_HELPER(uchar3, 3, unsigned char)
TEMPLATE_HELPER(short2, 2, short)
TEMPLATE_HELPER(short3, 3, short)
TEMPLATE_HELPER(ushort2, 2, unsigned short)
TEMPLATE_HELPER(ushort3, 3, unsigned short)
TEMPLATE_HELPER(float2, 2, float)
TEMPLATE_HELPER(float3, 3, float)
TEMPLATE_HELPER(double2, 2, double)
TEMPLATE_HELPER(double3, 3, double)