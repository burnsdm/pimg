//
// Created by david on 2/5/16.
//

template<typename PixelType,unsigned int Dim>
pImgOut<PixelType,Dim>::pImgOut()
{
    // this may not be required, but makes accessing the pointer safe from python even if not intialized
    this->img_out = ImageType::New();
};

template<typename PixelType,unsigned int Dim>
pImgOut<PixelType,Dim>::~pImgOut(){} // only data at present is smart pointers


template<typename PixelType,unsigned int Dim>
void pImgOut<PixelType,Dim>::SetOutputImage(typename ImageType::Pointer img)
{
    this->img_out = img;
    this->directions = img->GetDirection();
    this->origin = img->GetOrigin();
    this->spacing = img->GetSpacing();


}


template<typename PixelType,unsigned int Dim>
void pImgOut<PixelType,Dim>::CopyOutput(PixelType* scalars_cpout, int sz1, int sz2, int sz3)
{
    /* Copies pixel buffer from img_out into scalars */

    typename ImageType::SizeType imSize = this->img_out->GetLargestPossibleRegion().GetSize();
    if (imSize[0] != sz3 || imSize[1] != sz2 || (Dim == 2 && sz1 != 1) || (Dim == 3 && imSize[2] != sz1))
    {
        std::cerr<<"pImg::CopyOutput - invalid output image dimensions..." <<std::endl;
        std::cerr<<"Py: [" <<sz1 <<"," <<sz2 <<"," <<sz3 <<"]" <<std::endl;
        std::cerr<<"Cpp: " <<imSize <<std::endl;
        return;
    }
    memcpy(scalars_cpout,this->img_out->GetBufferPointer(),sizeof(PixelType)*sz1*sz2*sz3);
}

template<typename PixelType,unsigned int Dim>
void pImgOut<PixelType,Dim>::GetOutputPtr(PixelType** scalars_out, int* sz1, int* sz2, int* sz3)
{
    /* Returns reference to scalars for img_out - BEWARE if object goes out of scope so does the returned pointer */
    typename ImageType::SizeType imSize = this->img_out->GetLargestPossibleRegion().GetSize();
    *sz3=imSize[0];
    *sz2=imSize[1];
    if (Dim == 3)
        *sz1=imSize[2];
    else if (Dim == 2)
        *sz1 = 1;

    typename ImageType::PixelContainer* container;
    container = this->img_out->GetPixelContainer();
    container->SetContainerManageMemory(false);
    PixelType* pixBuff = container->GetImportPointer();
    *scalars_out = pixBuff;
}


template<typename PixelType,unsigned int Dim>
void pImgOut<PixelType,Dim>::GetSpacing(double spacing_out[3])
{
    /* Returns output image spacing */
    spacing_out[0] = this->spacing[0];
    spacing_out[1] = this->spacing[1];
    if (Dim == 3)
        spacing_out[2] = this->spacing[2];
    else if (Dim == 2)
        spacing_out[2] = 1.0;
}


template<typename PixelType,unsigned int Dim>
void pImgOut<PixelType,Dim>::GetOrigin(double origin_out[3])
{
    /* Returns output image origin */
    origin_out[0] = -this->origin[0]; // negative for IJK to RAS difference
    origin_out[1] = -this->origin[1]; // negative for IJK to RAS difference
    if (Dim == 3)
        origin_out[2] = this->origin[2];
    else if (Dim == 2)
        origin_out[2] = 0.0;

}

template<typename PixelType,unsigned int Dim>
void pImgOut<PixelType,Dim>::GetDirections(double directions_out[3][3])
{
    for (int i = 0; i< 3; i++)
        for (int j = 0; j< 3; j++)
            directions_out[i][j] = 0;

    for (int i = 0; i< Dim; i++)
        for (int j = 0; j< Dim; j++)
            directions_out[i][j] = this->directions[i][j];

    if (Dim == 2)
    {
        directions_out[2][2] = 1;
    }

    // Convert between ITK and VTK IJKtoRAS matrix storage
    directions_out[0][0] = -directions_out[0][0];
    directions_out[1][1] = -directions_out[1][1];

}

