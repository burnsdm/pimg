//
// Created by david on 2/5/16.
//

#ifndef PIMG_PIMGIN_H
#define PIMG_PIMGIN_H

#include <itkImage.h>
#include <itkImportImageFilter.h>

template<typename PixelType,unsigned int Dim>
class pImgIn {
public:

    // constructor/destructor
    pImgIn();
    ~pImgIn();

    // python interface inputs
    void SetScalars(PixelType* scalars, int sz1, int sz2, int sz3);
    void SetInputSpacing(double* spacing, int sz);
    void SetInputOrigin(double* origin, int sz);
    void SetInputDirections(double* directions, int sz1, int sz2);

    // C++ interface (do not copy into SWIG file)
    typename itk::Image<PixelType,Dim>::Pointer GetImage(){return img_in;}
    bool scalarsIsSet;
    bool originIsSet;
    bool spacingIsSet;

protected:
    typedef typename itk::Image<PixelType,Dim> ImageType;
    typedef typename itk::ImportImageFilter<PixelType,Dim> ImportFilterType;

    typename ImageType::Pointer img_in;
    typename ImportFilterType::Pointer importFilter;

};

#include "pImgIn.cpp"

#endif //PIMG_PIMGIN_H
