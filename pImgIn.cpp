//
// Created by david on 2/5/16.
//

#include "pImgIn.h"

template<typename PixelType,unsigned int Dim>
pImgIn<PixelType,Dim>::pImgIn()
{
    this->importFilter = ImportFilterType::New();
    this->spacingIsSet = 0;
    this->originIsSet = 0;
    this->scalarsIsSet = 0;
};

template<typename PixelType,unsigned int Dim>
pImgIn<PixelType,Dim>::~pImgIn(){} // only data at present is smart pointers

template<typename PixelType,unsigned int Dim>
void pImgIn<PixelType,Dim>::SetScalars(PixelType* scalars, int sz1, int sz2, int sz3)
{
    /* Assigns the img_in pixel buffer to scalar array (no copy)
       numpy uses reverse indexing, but c-style memory contiguity (i think)
       2D and 3D slicer images are 3D matrices */\

    typename ImportFilterType::SizeType imSize;
    imSize[0] = sz3;                     // X coordinate
    imSize[1] = sz2;                     // Y coordinate
    if (Dim == 3)
        imSize[2] = sz1;
    else if (Dim == 2 && sz1 != 1)    {  // Z coordinate
        std::cerr << "Invalid image dimensions for npImg<PixelType,2>::SetScalars " << std::endl;
        return;
    }

    typename ImportFilterType::IndexType start;
    start.Fill( 0 );
    typename ImportFilterType::RegionType region;
    region.SetIndex(start);
    region.SetSize(imSize);
    this->importFilter->SetRegion(region);

    typename ImageType::PointType origin;
    origin.Fill(0.0);
    this->importFilter->SetOrigin(origin);

    typename ImageType::SpacingType spacing;
    spacing.Fill(1.0);
    this->importFilter->SetSpacing(spacing);

    const bool importImageFilterWillOwnTheBuffer = false;
    this->importFilter->SetImportPointer(scalars, sz1*sz2*sz3,
                                         importImageFilterWillOwnTheBuffer);

    this->img_in=this->importFilter->GetOutput();
    this->importFilter->Update();
    this->img_in->Update();

    this->scalarsIsSet = 1;
}


template<typename PixelType,unsigned int Dim>
void pImgIn<PixelType,Dim>::SetInputSpacing(double* spacing, int sz)
{
    /* Sets the spacing of the input image */
    if (sz < Dim)
    {
        std::cerr<<"pImgIn::SetInputSpacing spacing arg invalid size " <<std::endl;
        return;
    }
    typename ImageType::SpacingType imgSpacing;
    for (int i = 0; i < Dim; i++)
        imgSpacing[i] = spacing[i];
    this->img_in->SetSpacing(imgSpacing);

    this->spacingIsSet = 1;
}

template<typename PixelType,unsigned int Dim>
void pImgIn<PixelType,Dim>::SetInputOrigin(double* origin, int sz)
{
    /* Sets the origin of the input image */
    if (sz < Dim)
    {
        std::cerr<<"pImgIn::SetInputOrigin spacing arg invalid size " <<std::endl;
        return;
    }
    typename ImageType::PointType imgOrigin;
    for (int i = 0; i < Dim; i++)
        imgOrigin[i] = origin[i];

    // Convert between ITK and VTK IJKtoRAS matrix storage
    imgOrigin[0]=-imgOrigin[0];
    imgOrigin[1]=-imgOrigin[1];

    this->img_in->SetOrigin(imgOrigin);
    this->originIsSet = 1;
}

template<typename PixelType,unsigned int Dim>
void pImgIn<PixelType,Dim>::SetInputDirections(double* directions, int sz1, int sz2)
{
    /* Sets the direction matrix of the input image */
    // todo: do we need to check for vector magnitude and orthagonality?
    typename ImageType::DirectionType imgDirections;
    if (sz1 == Dim && sz2 == Dim)
    {
        for (unsigned int i = 0; i<Dim; i++)
            for (unsigned int j = 0; j<Dim; j++)
                imgDirections[i][j] = directions[i+j*Dim];

        // Convert between ITK and VTK IJKtoRAS matrix storage
        imgDirections[0][0]=-imgDirections[0][0];
        imgDirections[1][1]=-imgDirections[1][1];

        this->img_in->SetDirection(imgDirections);
    }
    else
    {
        std::cerr<<"pImgIn::SetInputDirections invalid direction matrix " <<std::endl;
        return;

    }


}