//
// Created by david on 2/4/16.
//

#ifndef PIMG_PIMG_H
#define PIMG_PIMG_H

#include <itkImage.h>
#include <itkImportImageFilter.h>

template<typename PixelType,unsigned int Dim>
class pImg {
public:
    
    // constructor/destructor
    pImg();
    ~pImg();

    // python interface inputs
    void SetScalars(PixelType* scalars, int sz1, int sz2, int sz3);
    void SetInputSpacing(double* spacing, int sz);
    void SetInputOrigin(double* origin, int sz);

    // python interface outputs
    void CopyOutput(PixelType* scalars_cpout, int sz1, int sz2, int sz3);
    void GetOutputPtr(PixelType** scalars_out, int* sz1, int* sz2, int* sz3);
    void GetOutputSpacing(double spacing_out[3]);
    void GetOutputOrigin(double origin_out[3]);

    // example
    void MagGrad();

protected:
    typedef typename itk::Image<PixelType,Dim> ImageType;
    typedef typename itk::ImportImageFilter<PixelType,Dim> ImportFilterType;

    typename ImageType::Pointer img_in;
    typename ImageType::Pointer img_out;
    typename ImportFilterType::Pointer importFilter;

};


#endif //PIMG_PIMG_H
